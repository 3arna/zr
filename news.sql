/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50139
Source Host           : localhost:3306
Source Database       : zr

Target Server Type    : MYSQL
Target Server Version : 50139
File Encoding         : 65001

Date: 2010-06-10 15:26:36
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `news`
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` int(10) NOT NULL,
  `date` datetime DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `teaser` varchar(800) DEFAULT NULL,
  `text` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES ('1', '2010-06-26 15:11:39', 'BP: naftos vartojimas smuko daugiausiai nuo 1982 m.', 'Pasaulinis naftos vartojimas pernai smuko 1.2 mln. bareliu per diena tai antras paeiliui metinis nuosmukis ir didziausias nuo 1982 m. metineje pasaulio energetikos apzvalgoje teigia Didziosios Britanijos bendrove BP Plc.', 'BP teigia, kad pasaulio naftos gavyba pernai sumazejo 2 mln. bareliu per diena, arba 2,6 proc., tai taip pat didziausias nuosmukis nuo 1982 m.');
