/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50139
Source Host           : localhost:3306
Source Database       : zr

Target Server Type    : MYSQL
Target Server Version : 50139
File Encoding         : 65001

Date: 2010-08-27 16:25:16
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `apie`
-- ----------------------------
DROP TABLE IF EXISTS `apie`;
CREATE TABLE `apie` (
  `text` varchar(5000) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `lang` varchar(5) NOT NULL,
  PRIMARY KEY (`lang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of apie
-- ----------------------------
INSERT INTO `apie` VALUES ('Apie LT askdnakljaskd sdh fkjskj hgkjdfhg kjhskjh gkjsdgk jskjg kjshfkjhsdkjf ksd kjsdjkf ksdf skjdfh skj skjf skhfks ds sf sf sdf fsd', 'LT');

-- ----------------------------
-- Table structure for `draugai`
-- ----------------------------
DROP TABLE IF EXISTS `draugai`;
CREATE TABLE `draugai` (
  `vardas` varchar(50) NOT NULL,
  `link` varchar(500) DEFAULT NULL,
  `logo` varchar(40) DEFAULT NULL,
  `apieLT` varchar(500) DEFAULT NULL,
  `apieEN` varchar(500) DEFAULT NULL,
  `apieRU` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`vardas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of draugai
-- ----------------------------
INSERT INTO `draugai` VALUES ('DySwift', 'http://dyswift.ws', 'DySwift', 'apie LTFirma pradedanti reklamos versla', null, null);

-- ----------------------------
-- Table structure for `grojarastis`
-- ----------------------------
DROP TABLE IF EXISTS `grojarastis`;
CREATE TABLE `grojarastis` (
  `pavadinimas` varchar(50) DEFAULT NULL,
  `atlikejes` varchar(50) DEFAULT NULL,
  `link` varchar(50) DEFAULT NULL,
  `id` int(2) NOT NULL DEFAULT '0',
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of grojarastis
-- ----------------------------
INSERT INTO `grojarastis` VALUES ('Petey Pablo', 'Dance with me', 'Dance', '0', '2010-04-29');

-- ----------------------------
-- Table structure for `istorija`
-- ----------------------------
DROP TABLE IF EXISTS `istorija`;
CREATE TABLE `istorija` (
  `text` varchar(5000) CHARACTER SET cp1257 DEFAULT NULL,
  `lang` varchar(5) NOT NULL,
  PRIMARY KEY (`lang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of istorija
-- ----------------------------
INSERT INTO `istorija` VALUES ('Tvirtinama, kad Natanas Stablfyldas išrado radiją prieš Teslą ar Markonį, bet jo prietaisas atrodo veikė naudodamas indukcijos transliaciją labiau nei radijo transliaciją.\r\n1893 m. Sent Luise, Misūryje, Nikolas Tesla pademonstravo pirmąją radijo transliaciją. Sakydamas kalbą Filadelfijos Franklino Institute ir Nacionalinėje Elektros Šviesos Asocijacijoje jis apibūdino ir detaliai pademonstravo radijo ryšio veikimo principus. Aparate, kurį jis naudojo, buvo visi elementai, kurie buvo panaudoti radijo sistemose prieš pradedant naudoti vakuuminį vamzdelį.\r\n1894 m. Britanijos fizikas, seras Oliveris Lodžas pademonstravo ryšių, naudojant radijo bangas, susekimo įrenginį pavadintą bangų imtuvu naudojimo galimybę, vamzdžiu užpildytu geležies drožlėmis, kuris buvo išrastas Temistoklio Kalzechio-Onesčio Fermo mieste Italijoje 1884. Eduardas Branlis iš Prancūzijos ir Aleksandras Popovas iš Rusijos vėliau išleido pagerintas bangų imtuvo versijas. A. Popovas, kuris sukūrė praktinę komunikacijos sistemą paremtą bangų imtuvu, yra dažnai savo tėvynainių laikomas radijo išradėju.', 'LT');

-- ----------------------------
-- Table structure for `laidos`
-- ----------------------------
DROP TABLE IF EXISTS `laidos`;
CREATE TABLE `laidos` (
  `id` varchar(100) NOT NULL DEFAULT '0',
  `pavadinimas` varchar(100) DEFAULT NULL,
  `aprasimasLT` varchar(800) DEFAULT NULL,
  `aprasimasEN` varchar(800) DEFAULT NULL,
  `aprasimasRU` varchar(800) DEFAULT NULL,
  `dienos` varchar(100) DEFAULT NULL,
  `laikas` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of laidos
-- ----------------------------
INSERT INTO `laidos` VALUES ('laida1', 'Taskals', 'TaskalsLT Padainuokim tv show (dalyvauja supermuzika) vol. 1. flax650 143 videos. Subscribe Edit Subscription. Loading..Padainuokim tv show (dalyvauja supermuzika) vol. 1. flax650 143 videos. Subscribe Edit Subscription. Loading..', 'TaskalsEN', 'TaskalsRU', 'Pirmadieni Antradieni Treciadieni Ketvirtadieni Penktadieni', '20-22h');
INSERT INTO `laidos` VALUES ('laida2', 'Popiete', 'RutaLT Showcase.ca - The home of television without borders. Explore and watch your favourite Showcase series. ... Featured Shows. CalifornicationShowcase.ca - The home of television without borders. Explore and watch your favourite Showcase series. ... Featured Shows. Californication', 'RutaEN', 'RutaRU', 'shudas', null);
INSERT INTO `laidos` VALUES ('laida3', 'Krapstyk rura', 'Krapstyk rura LT Cancelled TV shows and TV series last episodes. Watch free online video, television cast reunions, podcasts, TV show DVDs, and where are they now?Cancelled TV shows and TV series last episodes. Watch free online video, television cast reunions, podcasts, TV show DVDs, and where are they now?', 'Krapstyk rura EN', 'Krapstyk rura RU', null, null);

-- ----------------------------
-- Table structure for `links`
-- ----------------------------
DROP TABLE IF EXISTS `links`;
CREATE TABLE `links` (
  `id` int(10) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `type` varchar(20) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  `number` int(10) DEFAULT NULL,
  `reg` varchar(20) DEFAULT NULL,
  `aboutEN` varchar(300) DEFAULT NULL,
  `aboutLT` varchar(300) DEFAULT NULL,
  `aboutRU` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of links
-- ----------------------------
INSERT INTO `links` VALUES ('1', 'facebook', 'Humoras', 'http://www.youtube.com', '1', 'true', 'apie EN1', 'about LT1', 'apie RU');
INSERT INTO `links` VALUES ('2', 'facebook', 'Skelbimai', 'http://www.skelbiu.lt', '1', 'true', 'about English kasdmakls', 'apie Lietuviskai asdasdasffa', 'apie Rusiskai asdasfasfasf');
INSERT INTO `links` VALUES ('3', 'facebook', 'Humoras', 'http://www.cha.lt', '1', 'true', 'apie EN2', 'about LT2', 'apie RU');
INSERT INTO `links` VALUES ('4', 'facebook', 'Humoras', 'http://www.cha.lt', '1', 'true', 'apie EN3', 'about LT3', 'apie RU');
INSERT INTO `links` VALUES ('5', 'facebook', 'Humoras', 'http://www.cha.lt', '1', 'true', 'apie EN', 'about LT4', 'apie RU');
INSERT INTO `links` VALUES ('6', 'facebook', 'Humoras', 'http://www.cha.lt', '1', 'true', 'apie EN', 'about LT5', 'apie RU');
INSERT INTO `links` VALUES ('7', 'facebook', 'Humoras', 'http://www.cha.lt', '1', 'true', 'apie EN', 'about LT6', 'apie RU');
INSERT INTO `links` VALUES ('8', 'facebook', 'Humoras', 'http://www.cha.lt', '1', 'true', 'apie EN', 'about LT7', 'apie RU');
INSERT INTO `links` VALUES ('9', 'facebook', 'Humoras', 'http://www.cha.lt', '1', 'true', 'apie EN', 'about LT8', 'apie RU');
INSERT INTO `links` VALUES ('10', 'facebook', 'Humoras', 'http://www.cha.lt', '1', 'true', 'apie EN', 'about LT9', 'apie RU');
INSERT INTO `links` VALUES ('11', 'facebook', 'Humoras', 'http://www.cha.lt', '1', 'true', 'apie EN', 'about LT10', 'apie RU');
INSERT INTO `links` VALUES ('12', 'facebook', 'Humoras', 'http://www.cha.lt', '1', 'true', 'apie EN', 'about LT11', 'apie RU');

-- ----------------------------
-- Table structure for `members`
-- ----------------------------
DROP TABLE IF EXISTS `members`;
CREATE TABLE `members` (
  `id` int(10) NOT NULL DEFAULT '0',
  `email` varchar(30) DEFAULT NULL,
  `password` varchar(40) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `flag` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of members
-- ----------------------------
INSERT INTO `members` VALUES ('1', 'liezas', '9820d7eafeefec47b2c7785d3abcac05', 'Zarna', '2010-04-22', '127.0.0.1', '1');
INSERT INTO `members` VALUES ('2', 'liezas', '81dc9bdb52d04dc20036dbd8313ed055', 'Zarna', '2010-04-22', '127.0.0.1', '1');
INSERT INTO `members` VALUES ('3', 'liezas', '81dc9bdb52d04dc20036dbd8313ed055', 'Zarna', '2010-04-22', '127.0.0.1', '1');
INSERT INTO `members` VALUES ('4', 'liezas', '9820d7eafeefec47b2c7785d3abcac05', 'Zarna', '2010-04-22', '127.0.0.1', '1');
INSERT INTO `members` VALUES ('5', 'liezas', '9820d7eafeefec47b2c7785d3abcac05', 'Zarna', '2010-04-22', '127.0.0.1', '0');
INSERT INTO `members` VALUES ('6', 'liezas', '9820d7eafeefec47b2c7785d3abcac05', 'Zarna', '2010-04-22', '127.0.0.1', '3');
INSERT INTO `members` VALUES ('7', 'liezas@gmail.com', '4bfc505bc3226c307bfc74082ce24886', 'Zarna', '2010-04-22', '127.0.0.1', '0');
INSERT INTO `members` VALUES ('8', 'Pimpis', '360cf9d24312b354195e4e4bf113c279', 'Pimpis', '2010-06-20', '127.0.0.1', '3');

-- ----------------------------
-- Table structure for `name`
-- ----------------------------
DROP TABLE IF EXISTS `name`;
CREATE TABLE `name` (
  `name` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of name
-- ----------------------------
INSERT INTO `name` VALUES ('Zarna');
INSERT INTO `name` VALUES ('1');

-- ----------------------------
-- Table structure for `news`
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` int(10) NOT NULL,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `poster` varchar(30) DEFAULT NULL,
  `titleLT` varchar(200) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `titleEN` varchar(200) DEFAULT NULL,
  `titleRU` varchar(200) DEFAULT NULL,
  `teaserLT` varchar(800) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `teaserEN` varchar(800) DEFAULT NULL,
  `teaserRU` varchar(800) DEFAULT NULL,
  `textLT` varchar(5000) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `textEN` varchar(5000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES ('1', '2010-06-26', '13:36:58', 'Petras', 'Velnio advokatai apie \"Majonezo\" skoni', null, null, 'Psichoterapeutas Olegas Lapinas ir buves zurnalistas Audrius Lelkaitis videobloge „Velnio advokatai\" apzvelgia svarbiausius ivykius, kovodami su vidine ir isorine cenzura. si karta jie kalba apie majonezo skoni.', null, null, 'BP teigia, kad pasaulio naftos gavyba pernai sumazejo 2 mln. bareliu per diena, arba 2,6 proc., tai taip pat didziausias nuosmukis nuo 1982 m.', null);
INSERT INTO `news` VALUES ('2', '2010-06-27', '17:37:01', 'Jonas', '2BP: naftos vartojimas smuko daugiausiai nuo 1982 m.', null, null, 'Pasaulinis naftos vartojimas pernai smuko 1.2 mln. bareliu per diena tai antras paeiliui metinis nuosmukis ir didziausias nuo 1982 m. metineje pasaulio energetikos apzvalgoje teigia Didziosios Britanijos bendrove BP Plc.', null, null, 'BP teigia, kad pasaulio naftos gavyba pernai sumazejo 2 mln. bareliu per diena, arba 2,6 proc., tai taip pat didziausias nuosmukis nuo 1982 m.', null);
INSERT INTO `news` VALUES ('3', '2010-06-28', '07:37:06', 'Juozapas', '3BP: naftos vartojimas smuko daugiausiai nuo 1982 m.', null, null, 'Pasaulinis naftos vartojimas pernai smuko 1.2 mln. bareliu per diena tai antras paeiliui metinis nuosmukis ir didziausias nuo 1982 m. metineje pasaulio energetikos apzvalgoje teigia Didziosios Britanijos bendrove BP Plc.', null, null, 'BP teigia, kad pasaulio naftos gavyba pernai sumazejo 2 mln. bareliu per diena, arba 2,6 proc., tai taip pat didziausias nuosmukis nuo 1982 m.', null);
INSERT INTO `news` VALUES ('4', '2010-06-29', '13:37:11', 'Kastytis', '4BP: naftos vartojimas smuko daugiausiai nuo 1982 m.', null, null, 'Pasaulinis naftos vartojimas pernai smuko 1.2 mln. bareliu per diena tai antras paeiliui metinis nuosmukis ir didziausias nuo 1982 m. metineje pasaulio energetikos apzvalgoje teigia Didziosios Britanijos bendrove BP Plc.', null, null, 'BP teigia, kad pasaulio naftos gavyba pernai sumazejo 2 mln. bareliu per diena, arba 2,6 proc., tai taip pat didziausias nuosmukis nuo 1982 m.', null);
INSERT INTO `news` VALUES ('5', '2010-06-30', '08:37:17', 'Cvenkeris', '5BP: naftos vartojimas smuko daugiausiai nuo 1982 m.', null, null, 'Pasaulinis naftos vartojimas pernai smuko 1.2 mln. bareliu per diena tai antras paeiliui metinis nuosmukis ir didziausias nuo 1982 m. metineje pasaulio energetikos apzvalgoje teigia Didziosios Britanijos bendrove BP Plc.', null, null, 'BP teigia, kad pasaulio naftos gavyba pernai sumazejo 2 mln. bareliu per diena, arba 2,6 proc., tai taip pat didziausias nuosmukis nuo 1982 m.', null);
INSERT INTO `news` VALUES ('6', '2010-07-01', '10:31:06', 'Zarna', 'Radijos išradimas ir istorija', null, null, 'Teorinį pagrindą elektromagnetinių bangų sklidimo aprašymui 1873 metais padėjo Džeimsas Klarkas Maksvelas savo laiške Karališkajai draugijai Elektromagnetinių bangų dinaminė teorija, kuris sekė iš jo darbo tarp 1861 ir 1865.\r\nHein', null, null, '1893 m. Sent Luise, Misūryje, Nikolas Tesla pademonstravo pirmąją radijo transliaciją. Sakydamas kalbą Filadelfijos Franklino Institute ir Nacionalinėje Elektros Šviesos Asocijacijoje jis apibūdino ir detaliai pademonstravo radijo ryšio veikimo principus. Aparate, kurį jis naudojo, buvo visi elementai, kurie buvo panaudoti radijo sistemose prieš pradedant naudoti vakuuminį vamzdelį.\r\n1894 m. Britanijos fizikas, seras Oliveris Lodžas pademonstravo ryšių, naudojant radijo bangas, susekimo įrenginį pavadintą bangų imtuvu naudojimo galimybę, vamzdžiu užpildytu geležies drožlėmis, kuris buvo išrastas Temistoklio Kalzechio-Onesčio Fermo mieste Italijoje 1884. Eduardas Branlis iš Prancūzijos ir Aleksandras Popovas iš Rusijos vėliau išleido pagerintas bangų imtuvo versijas. A. Popovas, kuris sukūrė praktinę komunikacijos sistemą paremtą bangų imtuvu, yra dažnai savo tėvynainių laikomas radijo išradėju.\r\n1896 m. Giljelmui Markoniui buvo suteiktas D. Britanijos patentas nr. 12039, Elektrinių impulsų ir signalų perdavimo aparatuose patobulinimas; kartais laikoma, jog tai yra pirmasis patentas radijo imtuvui. 1897 m. Nikola Tesla Jungtinėse Valstijose sukūrė ir užpatentavo, kai kuriuos tonacijos patobulinimus. JAV Patentų Biuras priešingai jų sprendimui 1904 m., Giljelmui Markoniui suteikė radijo išradimo patentą, greičiausiai įtakotas Markonio finansinių rėmėjų JAV, tarp kurių buvo Tomas Edisonas ir Endriu Karnegis. 1909 Markonis su Karlu Ferdinandu Braunu, buvo apdovanoti Nobelio Premija už Fiziką už „įnašą tobulinant radijo telegrafiją“. Kaip ten bebūtų, Teslos patentas (nr. 645576) buvo 1943 m. JAV Aukščiausiojo Teismo buvo pripažintas pirmesniu už Markonio, tačiau tai atsitiko jau po Teslos mirties. Kai kurie autoriai mano, kad tai buvo padaryta dėl finansinių priežasčių, kad leistų JAV Vyriausybei išvengti nuostolių apmokėjimo, kurių reikalavo Markonio kompanijos už jų patentų naudojimą per Pirmą Pasaulinį Karą.', null);

-- ----------------------------
-- Table structure for `reklama`
-- ----------------------------
DROP TABLE IF EXISTS `reklama`;
CREATE TABLE `reklama` (
  `text` varchar(500) CHARACTER SET utf8 COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `lang` varchar(10) NOT NULL,
  PRIMARY KEY (`lang`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of reklama
-- ----------------------------

-- ----------------------------
-- Table structure for `sarasas`
-- ----------------------------
DROP TABLE IF EXISTS `sarasas`;
CREATE TABLE `sarasas` (
  `nr` int(10) NOT NULL,
  `vedejas` varchar(100) NOT NULL DEFAULT '0',
  `laida` varchar(100) DEFAULT NULL,
  `diena` varchar(20) DEFAULT NULL,
  `starts` time DEFAULT NULL,
  `ends` time DEFAULT NULL,
  PRIMARY KEY (`nr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of sarasas
-- ----------------------------
INSERT INTO `sarasas` VALUES ('1', 'vedejas0', 'laida1', 'Pirmadienis', '19:30:00', '20:30:00');
INSERT INTO `sarasas` VALUES ('3', 'vedejas0', 'laida2', 'Pirmadienis', '20:30:00', '21:30:00');
INSERT INTO `sarasas` VALUES ('4', 'vedejas0', 'laida3', 'Pirmadienis', '13:00:00', '15:30:00');
INSERT INTO `sarasas` VALUES ('5', 'vedejas0', 'laida2', 'Antradienis', '19:30:00', '20:30:00');
INSERT INTO `sarasas` VALUES ('6', 'vedejas1', 'laida2', 'Antradienis', '19:30:00', '20:30:00');
INSERT INTO `sarasas` VALUES ('7', 'vedejas1', 'laida2', 'Treciadienis', '12:13:11', '19:30:00');
INSERT INTO `sarasas` VALUES ('8', 'vedejas2', 'laida2', 'Pirmadienis', '18:30:00', '19:30:00');
INSERT INTO `sarasas` VALUES ('9', 'vedejas3', 'laida2', 'Antradienis', '19:30:00', '20:30:00');

-- ----------------------------
-- Table structure for `vedejai`
-- ----------------------------
DROP TABLE IF EXISTS `vedejai`;
CREATE TABLE `vedejai` (
  `id` varchar(20) NOT NULL DEFAULT '0',
  `vardas` varchar(30) DEFAULT NULL,
  `pavarde` varchar(30) DEFAULT NULL,
  `age` varchar(20) DEFAULT NULL,
  `town` varchar(50) DEFAULT NULL,
  `bands` varchar(300) DEFAULT NULL,
  `music` varchar(300) DEFAULT NULL,
  `aboutLT` varchar(800) DEFAULT NULL,
  `aboutEN` varchar(800) DEFAULT NULL,
  `aboutRU` varchar(800) DEFAULT NULL,
  `nuotrauka` varchar(800) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of vedejai
-- ----------------------------
INSERT INTO `vedejai` VALUES ('vedejas0', 'Negras', 'Lincolnas', '20', 'Telsiai', 'Dj Tiesto', 'House, Dance, Trance, Pop', 'Negras', 'NigerEN', 'NigerRu', 'Niger.jpg');
INSERT INTO `vedejai` VALUES ('vedejas1', 'Ruta', 'Keksaite', '21', 'Vilnius', 'DJ Antoine', 'Techhouse, Minimal, Electro', 'Durna', 'Stupid', 'Savalka', 'Ruta.png');
INSERT INTO `vedejai` VALUES ('vedejas2', 'Petras', 'Cesnauskas', '22', 'Kaunas', 'Broliai aliukai ir sesute', 'D&B, Hip hop, R&B', 'Durns', 'Stupid', 'Lox', 'Petras.jpg');
INSERT INTO `vedejai` VALUES ('vedejas3', 'Vaidas', 'Kalabibiskius', '23', 'Klaipeda', 'Britney spers', 'Rock, Jazz, Pop', 'Daunas', 'Daun', 'Daunox', 'Vaidas.jpg');
INSERT INTO `vedejai` VALUES ('vedejes4', 'Virejes', 'Virtuvinis', '24', 'Siauliai', 'Book Shade', 'House, Trance, Electro house', 'Verda', 'Cook', 'Gadyca', 'Virejas.jpg');
